<?php

require_once(__DIR__ . '/bootstrap.php');

/** Количество процессов для запуска. */
const PROC_COUNT = 150;

/** Количество операций чтения для проверки. */
const READ_COUNT = 10000;

/** Подключаем окружение для тестового кэша. */
$behavior = new TextMedia\ShmCache\Tests\TestBehavior;

/** Читаем данные и ключи. */
$data  = $behavior->getData();
$keys  = array_keys($data->getArrayCopy());
$count = count($keys);

/** Кэш в разделяемой памяти. */
$shmCache = new TextMedia\ShmCache\Cache($behavior);

/** Кэш в memcached. */
$memcached = new Memcached;
$memcached->addServer(MEMCACHED_HOST, MEMCACHED_PORT);

$param = $argv[1] ?? null;
switch ($param) {
    case TEST_SHMCACHE:
        // Ждем прогрева.
        while (!$shmCache->isReady()) {
            usleep(10000); // 0.01s
        }
        // Читаем таблицу (исключаем из тайминга).
        $shmCache->getOffset();
        // Выполняем чтение.
        $time = microtime(true);
        for ($num = 0; $num < READ_COUNT; $num++) {
            $key = $keys[rand() % $count];
            $shmCache->getItem($key);
        }
        // Выводим время.
        printf("%10s: %1.8f\n", $param, microtime(true) - $time);
        break;
    case TEST_MEMCACHED:
        // Ждем прогрева.
        while (1 != $memcached->get('status')) {
            usleep(10000); // 0.01s
        }
        // Выполняем чтение.
        $time = microtime(true);
        for ($num = 0; $num < READ_COUNT; $num++) {
            $key = $keys[rand() % $count];
            $memcached->get($key);
        }
        // Выводим время.
        printf("%10s: %1.8f\n", $param, microtime(true) - $time);
        break;
    default:
        /** Файл в который будут выводиться данные тестирования. */
        $logFile = sys_get_temp_dir() . '/ShmCacheTestPerformance.log';
        file_put_contents($logFile, '');

        /** Шаблон команды для запуска процессов тестирования. */
        $command = "php " . __FILE__ . " %s >> {$logFile} 2>&1 &";

        // ShmCache: чистим кэш, запускаем процессы чтения.
        $shmCache->clean(true);
        for ($num = 0; $num < PROC_COUNT; $num++) {
            exec(sprintf($command, TEST_SHMCACHE));
            printf(" Processes: %4d\r", $num + 1);
        }

        // Memcached: чистим кэш, запускаем процессы чтения.
        $memcached->set('status', 0, time() + 3600);
        for ($num = 0; $num < PROC_COUNT; $num++) {
            exec(sprintf($command, TEST_MEMCACHED));
            printf(" Processes: %4d\r", $num + 1 + PROC_COUNT);
        }

        // Итоговые данные.
        $result = [
            TEST_SHMCACHE  => [0, 0, 0, PHP_INT_MAX],
            TEST_MEMCACHED => [0, 0, 0, PHP_INT_MAX],
        ];

        // Прогреваем оба кэша.
        $time = microtime(true);
        foreach ($data as $key => $value) {
            $memcached->set($key, $value, time() + 3600);
        }
        $result[TEST_MEMCACHED][4] = microtime(true) - $time;
        $time = microtime(true);
        $shmCache->warmup(false);
        $result[TEST_SHMCACHE][4] = microtime(true) - $time;
        $memcached->set('status', 1, time() + 3600);

        // Ждем пока все процессы не завершаться.
        $command = 'ps -Af | grep -P "' . __FILE__ . ' (' . TEST_MEMCACHED . '|' . TEST_SHMCACHE . ')" | wc -l';
        do {
            usleep(100000); // 0.1s
            $count = (int) `$command`;
            printf(" Processes: %4d\r", $count);
        } while ($count > 0);
        echo str_repeat(' ', 20) . "\r";

        // Обработаем лог.
        $max = max((int) $result[TEST_MEMCACHED][4], (int) $result[TEST_SHMCACHE][4]);
        foreach (file($logFile) as $line) {
            $line = preg_replace('#[\r\n\s\t]+#', '', $line);
            if (!empty($line)) {
                list($type, $time) = explode(':', $line);
                $result[$type][0]++;
                $result[$type][1] += (float) $time;
                $result[$type][2] = max($result[$type][2], (float) $time);
                $result[$type][3] = min($result[$type][3], (float) $time);
                $max = max($max, (int) $result[$type][2]);
            }
        }
        $format = ' = %' . (9 + strlen("{$max}")) . '.8f';
        $format = "%10s: avg{$format} | max{$format} | min{$format} | warmup{$format}\n";
        foreach ($result as $type => $data) {
            $data[1] /= $data[0];
            printf($format, $type, $data[1], $data[2], $data[3], $data[4]);
        }
        unlink($logFile);
}
