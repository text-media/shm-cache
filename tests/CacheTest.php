<?php

namespace TextMedia\ShmCache\Tests;

use Memcached;
use TextMedia\ShmCache\Cache;

/**
 * Тесты.
 */
class CacheTest extends \PHPUnit\Framework\TestCase
{
    /** @var \TextMedia\ShmCache\Behavior Тестовый кэш. */
    protected static $behavior;

    /** @var \TextMedia\ShmCache\Cache Тестовый кэш. */
    protected static $cache;

    /** @var \Memcached Кэш в memcached. */
    protected static $memcached;

    /** @var array Время выполнения всех тестов по каждому типу. */
    protected static $timing = [TEST_SHMCACHE => 0, TEST_MEMCACHED => 0];

    /** @var boolean Следует ли выполнять проверку производительности. */
    protected static $performance = true;

    /**
     * Перед тестами...
     */
    public static function setUpBeforeClass()
    {
        if (empty(self::$behavior)) {
            // Определим необходимость проверки производительности.
            self::$performance = (false === strpos(getenv('PHP_UNIT'), '--no-performance'));

            // Прогреем кэш в разделяемой памяти.
            self::$behavior = new TestBehavior();
            self::$cache    = new Cache(self::$behavior);
            self::$cache->warmup(true);

            // Прогреем memcached.
            self::$memcached = new Memcached();
            self::$memcached->addServer(MEMCACHED_HOST, MEMCACHED_PORT);
            $data = self::$behavior->getData();
            foreach ($data as $key => $value) {
                self::$memcached->set($key, $value, time() + 100);
            }
        }
    }

    /**
     * В конце тестов...
     */
    public static function tearDownAfterClass()
    {
        // Удаляем кэш из памяти.
        self::$cache->remove();

        // Запускаем тест производительности.
        // Выводим время SchCache, Memcached.
        echo "\n\nPerformance:\n";
        foreach (self::$timing as $key => $val) {
            printf("%10s: %11.8f\n", $key, $val);
        }
        if (self::$performance) {
            passthru('php ' . __DIR__ . '/TestPerformance.php');
        }
    }

    /**
     * Проверка работы кэша.
     *
     * @param string $key   Какой ключ должны прочитать из кэша.
     * @param mixed  $value Какой результат должны получить.
     *
     * @dataProvider cacheProvider
     */
    public function testCache(string $key, $value)
    {
        // Проверим работу ShmCache.
        $time = microtime(true);
        $cached = self::$cache->getItem($key);
        $this->assertEquals($cached, $value);
        self::$timing[TEST_SHMCACHE] += (microtime(true) - $time);

        // Проверим работу Memcached.
        $time = microtime(true);
        $cached = self::$memcached->get($key);
        $this->assertEquals($cached, $value);
        self::$timing[TEST_MEMCACHED] += (microtime(true) - $time);
    }

    /**
     * Поставщик тестов запросов к кэшу.
     *
     * @return array
     */
    public function cacheProvider(): array
    {
        // В документации сказано, что setUpBeforeClass вызывается до тестов,
        // но какого-то лешего поставщик самих тестов вызывается до него.
        // Пришлось костылить.
        self::setUpBeforeClass();

        $result = [];
        foreach (self::$behavior->getData() as $key => $value) {
            $result[] = [$key, $value];
        }
        shuffle($result);
        return $result;
    }
}
