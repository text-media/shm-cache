<?php

namespace TextMedia\ShmCache\Tests;

use ArrayObject;
use TextMedia\ShmCache\Behavior;

/**
 * Тестовый кэш.
 */
class TestBehavior extends Behavior
{
    /** Идентификатор проекта. */
    const PROJECT_ID = 'z';

    /** Размер блока разделяемой памяти. */
    const CACHE_SIZE = '100m';

    /** Максимальный размер длины строкового представления данных. */
    const VALUE_SIZE = 2;

    /**
     * Получение данных для упаковки.
     *
     * @return \ArrayObject
     */
    public function getData(): ArrayObject
    {
        static $result = null;

        if (is_null($result)) {
            $result = [];
            for ($num = 1; $num <= 100; $num++) {
                // Данные по числовым ключам содержат строки.
                $result[$num] = "x{$num}x";
                // По строковым - большие массивы вида:
                //  - "x" - число (2 байта);
                //  - "y" - квадрат числа (3 байта);
                //  - "z" - несколько произвольных чисел длиной в 2 байта.
                $array = array_fill(0, rand() % 0xF, null);
                $array = array_map(function () {
                    return rand() % 0xFFFF;
                }, $array);
                $result["x{$num}x"] = [
                    'x' => $num,
                    'y' => $num * $num,
                    'z' => $array,
                ];
            }
            $result = new ArrayObject($result);
        }

        return $result;
    }

    /**
     * Упаковка элемента данных в строку для записи.
     *
     * @param string $key  Ключ (может повлиять на алгоритм упаковки).
     * @param mixed  $data Данные.
     *
     * @return string
     */
    public function packData(string $key, $data): string
    {
        return ($key{0} === 'x')
            ? (self::packNumber($data['x'], 2)
                . self::packNumber($data['y'], 3)
                . implode('', array_map(function ($z) {
                    return self::packNumber($z, 2);
                }, $data['z'])))
            : parent::packData($key, $data);
    }

    /**
     * Распаковка прочитанных из памяти данных в исходную структуру.
     *
     * @param string $key    Ключ (может повлиять на алгоритм упаковки).
     * @param string $packed Упакованные данные.
     *
     * @return mixed
     */
    public function unpackData(string $key, string $packed)
    {
        if ($key{0} === 'x') {
            $result = [
                'x' => self::unpackNumber(substr($packed, 0, 2)),
                'y' => self::unpackNumber(substr($packed, 2, 3)),
                'z' => [],
            ];
            $packed = substr($packed, 5);
            for ($num = 0, $len = strlen($packed); $num < $len; $num += 2) {
                $result['z'][] = self::unpackNumber(substr($packed, $num, 2));
            }
            return $result;
        } else {
            return parent::unpackData($key, $packed);
        }
    }
}
