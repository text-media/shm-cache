<?php
/**
 * Начальный загрузчик для тестов.
 * Необходим, т.к. тест производительности выполняется как отдельный процесс,
 * которому так же необходим автозагрузчик классов, настройки Memcached и проч.
 */

// Подключаем автозагрузчик классов.
require_once(__DIR__ . '/../vendor/autoload.php');

/** Хост memcached */
const MEMCACHED_HOST = '127.0.0.1';

/** Порт memcached */
const MEMCACHED_PORT = 11211;

/** Аргумент команды для тестирования ShmCache. */
const TEST_SHMCACHE = 'ShmCache';

/** Аргумент команды для тестирования Memcached. */
const TEST_MEMCACHED = 'MemCached';
