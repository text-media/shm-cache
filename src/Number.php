<?php

namespace TextMedia\ShmCache;

/**
 * Примесь для упаковки/распаковки числовых данных.
 * Вынесено в примесь, т.к. методы используются и в Cache, и в Behavior.
 */
trait Number
{
    /**
     * Упаковка числа в последовательность символов.
     *
     * @param integer $number Число.
     * @param integer $size   Длина итоговой строки.
     *
     * @return string
     */
    public static function packNumber(int $number, int $size): string
    {
        $string = '';
        for ($num = 0; $num < $size; $num++) {
            $string .= chr($number % 0x100);
            $number = (int) ($number / 0x100);
        }
        return $string;
    }

    /**
     * Распаковка последовательности символов в число.
     *
     * @param string $string
     *
     * @return integer
     */
    public static function unpackNumber(string $string): int
    {
        $number     = 0;
        $multiplier = 1;
        for ($num = 0, $len = strlen($string); $num < $len; $num++) {
            $number += ord($string{$num}) * $multiplier;
            $multiplier *= 0x100;
        }
        return $number;
    }
}
