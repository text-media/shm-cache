<?php

namespace TextMedia\ShmCache;

use ArrayObject;
use stdClass;

/**
 * Абстракция для настройки кэша и обработки данных в нем.
 *
 * Все константы должны быть переопределены, т.к. в абстратном виде содержат невалидные данные.
 * Методы, обязательные для реализации в потомках: getData.
 * Методы, которые могут быть переопределены: packData, unpackData, onCorrupt.
 */
abstract class Behavior
{
    use Number;

    /* ****************************************************************************************** *\
     * КОНСТАНТЫ
    \* ****************************************************************************************** */

    /**
     * Идентификатор проекта.
     * Это должен быть один символ (см. http://php.net/manual/ru/function.ftok.php).
     */
    const PROJECT_ID = '';

    /**
     * Размер блока разделяемой памяти.
     * Может быть задан целым числом байт или строкой типа: 1m, 100k и т.п.
     * Должен быть не менее 1 Кб.
     */
    const CACHE_SIZE = 0;

    /**
     * Максимальный размер длины строкового представления данных.
     * Определяет, сколько нужно байт для хранения длины упакованных данных.
     * Может меняться в пределах от 1 до 4.
     */
    const VALUE_SIZE = 0;

    /* ****************************************************************************************** *\
     * ПОЛЯ
    \* ****************************************************************************************** */

    /** @var integer Идентификатор блока разделяемой памяти. */
    protected $id = null;

    /** @var integer Размер блока разделяемой памяти. */
    protected $size = null;

    /** @var integer Максимальный размера длины строкового представления данных. */
    protected $length = null;

    /** @var string Имя объекта настроек (имя класса). */
    protected $name;

    /* ****************************************************************************************** *\
     * НЕИЗМЕНЯЕМЫЕ МЕТОДЫ
    \* ****************************************************************************************** */

    /**
     * Конструктор (обрабатывает константы и подгатавливает поля).
     *
     * @throws \TextMedia\ShmCache\Exception Если в константе указано неправильное значение.
     */
    final public function __construct()
    {
        static $ids = [], $sizes = [], $lengths = [];

        $class = get_called_class();

        // Подготовка системного идентификатора блока разделяемой памяти.
        if (!isset($ids[$class])) {
            $sym = $class::PROJECT_ID;
            if (strlen($sym) !== 1) {
                throw new Exception('Неправильное значение идентификатора проекта.', Exception::INVALID_PROJECT_ID);
            }

            $id = @ftok(__FILE__, $sym);
            if (-1 === $id) {
                throw new Exception('Не удалось определить идентификатор блока разделяемой памяти.', Exception::FAILED_GET_SHM_ID);
            }

            $ids[$class] = $id;
        }

        // Подготовка размера блока разделяемой памяти.
        if (!isset($sizes[$class])) {
            $size = $class::CACHE_SIZE;
            if (!is_scalar($size) or !preg_match('#^(\d+)([km]?)$#i', "{$size}", $match)) {
                throw new Exception('Неправильное значение размера блока разделяемой памяти.', Exception::INVALID_CACHE_SIZE);
            }

            $size = (int) $match[1];
            switch (strtolower($match[2])) {
                case 'k':
                    $size *= 0x400;
                    break;
                case 'm':
                    $size *= 0x100000;
                    break;
            }

            if ($size < 0x400) {
                throw new Exception('Слишком маленькое значение размера блока разделяемой памяти.', Exception::INVALID_CACHE_SIZE);
            }

            // Размер кэша = указанный + размер заголовка.
            $sizes[$class] = $size + Cache::HEADER_SIZE;
        }

        // Подготовка максимального размера длины строкового представления данных.
        if (!isset($lengths[$class])) {
            $length = $class::VALUE_SIZE;
            if (!is_int($length) or 1 > $length or $length > 4) {
                throw new Exception('Неправильное значение размера длины строкового представления данных.', Exception::INVALID_VALUE_SIZE);
            }

            $lengths[$class] = $length;
        }

        $this->id     = $ids[$class];
        $this->size   = $sizes[$class];
        $this->length = $lengths[$class];
        $this->name   = $class;

        call_user_func_array([$this, 'init'], func_get_args());
    }

    /**
     * Возврат системного идентификатора блока разделяемой памяти.
     *
     * @return integer
     */
    final public function getId(): int
    {
        return $this->id;
    }

    /**
     * Возврат размера блока разделяемой памяти.
     *
     * @return integer
     */
    final public function getSize(): int
    {
        return $this->size;
    }

    /**
     * Возврат максимального размера длины строкового представления данных.
     *
     * @return integer
     */
    final public function getLength(): int
    {
        return $this->length;
    }

    /**
     * Получение имени объекта настроек (имя класса).
     *
     * @return string
     */
    final public function getName(): string
    {
        return $this->name;
    }

    /* ****************************************************************************************** *\
     * АБСТРАКТНЫЕ МЕТОДЫ
    \* ****************************************************************************************** */

    /**
     * Получение данных для упаковки.
     *
     * @return \ArrayObject
     */
    abstract public function getData(): ArrayObject;

    /* ****************************************************************************************** *\
     * ПЕРЕОПРЕДЕЛЯЕМЫЕ МЕТОДЫ
    \* ****************************************************************************************** */

    /** Дополнительные действия для конструктора. */
    protected function init() {}

    /**
     * Упаковка элемента данных в строку для записи.
     * По умолчанию просто приводит к строке.
     *
     * @param string $key  Ключ (может повлиять на алгоритм упаковки).
     * @param mixed  $data Данные.
     *
     * @throws \TextMedia\ShmCache\Exception Если нельзя привести к строке.
     *
     * @return string
     */
    public function packData(string $key, $data): string
    {
        unset($key);
        if (is_scalar($data) or (is_object($data) and is_callable([$data, '__toString']))) {
            return (string) $data;
        } else {
            throw new Exception('Не удалось привести значение к строке.', Exception::FAILED_PACK_VALUE);
        }
    }

    /**
     * Распаковка прочитанных из памяти данных в исходную структуру.
     * По умолчанию просто возвращает as-is.
     *
     * @param string $key    Ключ (может повлиять на алгоритм распаковки).
     * @param string $packed Упакованные данные.
     *
     * @throws \TextMedia\ShmCache\Exception Если при распаковке произошла ошибка.
     *
     * @return mixed
     */
    public function unpackData(string $key, string $packed)
    {
        unset($key);
        return $packed;
    }

    /* ****************************************************************************************** *\
     * ОБРАБОТЧИКИ СОБЫТИЙ
    \* ****************************************************************************************** */

    /**
     * Поведение при обнаружении повреждения данных.
     * По умолчанию переадресует вызов на onEmpty().
     *
     * @param \TextMedia\ShmCache\Cache $cache Кэш.
     * @param string                    $key   Ключ.
     * @param string                    $value Данные.
     */
    public function onCorrupt(Cache $cache, string $key, string $value)
    {
        unset($key, $value);
        $this->onEmpty($cache);
    }

    /**
     * Поведение при обнаружении отсутствия данных.
     * По умолчанию не выполняет никаких действий.
     *
     * @param \TextMedia\ShmCache\Cache $cache Кэш.
     */
    public function onEmpty(Cache $cache)
    {
        unset($cache);
    }

    /**
     * Поведение после "прогрева".
     * По умолчанию не выполняет никаких действий.
     *
     * Объекты состояний содержат следующие поля:
     *  - float   $time:    когда был запущен/завершен "прогрев" (см. microtime(true));
     *  - integer $memory:  используемая память на момент запуска/завершения (см. memory_get_peak_usage(true));
     *  - integer $records: число записанных в память записей (на момент старта = 0);
     *  - integer $size:    сколько байт было записано в память (на момент старта = 0).
     *
     * @param \TextMedia\ShmCache\Cache $cache   Кэш.
     * @param \stdClass                 $onStart Состоняние на момент старта "прогрева".
     * @param \stdClass                 $onReady Состояние на момент его завершения.
     */
    public function onWarmed(Cache $cache, stdClass $onStart, stdClass $onReady)
    {
        unset($cache, $onStart, $onReady);
    }

    /**
     * Поведение после инициализации таблицы индексов (смещений).
     * По умолчанию не выполняет никаких действий.
     *
     * Объекты состояний содержат следующие поля:
     *  - float   $time:    когда был запущено/завершено чтения таблицы (см. microtime(true));
     *  - integer $memory:  используемая память на момент запуска/завершения (см. memory_get_peak_usage(true));
     *  - integer $records: число прочитанных из памяти индексов (на момент старта = 0);
     *  - integer $size:    сколько байт было записано в память (на момент старта = 0).
     *
     * @param \TextMedia\ShmCache\Cache $cache   Кэш.
     * @param \stdClass                 $onStart Состоняние на момент начала чтения таблицы.
     * @param \stdClass                 $onReady Состояние на момент завершения.
     */
    public function onIndexed(Cache $cache, stdClass $onStart, stdClass $onReady)
    {
        unset($cache, $onStart, $onReady);
    }
}
