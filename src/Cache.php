<?php

namespace TextMedia\ShmCache;

use ArrayObject;

/**
 * Кэш в разделяемой памяти.
 */
final class Cache
{
    use Number;

    /* ****************************************************************************************** *\
     * КОНСТАНТЫ
    \* ****************************************************************************************** */

    /** Статус кэша = пусто. */
    const STATUS_EMPTY = 0;

    /** Статус кэша = "прогревается". */
    const STATUS_WARMUP = 1;

    /** Статус кэша = готово к использованию. */
    const STATUS_READY = 2;

    /** Статус кэша = данные повреждены. */
    const STATUS_CORRUPT = 3;

    /** Число байт, отведенное под статус. */
    const STATUS_SIZE = 1;

    /**
     * Размер заголовка (смещение данных от начала блока).
     * Зарезервируем на будущее 1 килобайт.
     */
    const HEADER_SIZE = 0x400;

    /* ****************************************************************************************** *\
     * СОЗДАНИЕ И УДАЛЕНИЕ КЭША
    \* ****************************************************************************************** */

    /** @var \TextMedia\ShmStorage\Behavior Объект настроек/поведения кэша. */
    protected $behavior;

    /** @var boolean Флаг включения режима отладки. */
    protected $debugFlag = false;

    /** @var array Данные режима отладки. */
    protected $debugData = [];

    /**
     * Конструктор.
     *
     * @param \TextMedia\ShmStorage\Behavior $behavior Объект настроек/поведения кэша.
     */
    public function __construct(Behavior $behavior)
    {
        $this->behavior = $behavior;
    }

    /**
     * Подключение к блоку разделяемой памяти.
     *
     * @throws \TextMedia\ShmStorage\Exception
     *
     * @return resource Ресурс блока разделяемой памяти
     */
    public function open()
    {
        static $resources = [];

        $class = $this->getBehavior()->getName();
        if (!isset($resources[$class])) {
            $resource = @shmop_open($this->getBehavior()->getId(), 'c', 0666, $this->getBehavior()->getSize());
            if (!is_resource($resource)) {
                throw new Exception('Не удалось открыть блок разделяемой памяти.', Exception::FAILED_OPEN_SHMOP);
            }

            $resources[$class] = $resource;
        }

        return $resources[$class];
    }

    /**
     * Очистка кэша ото всех данных.
     *
     * @param boolean $full OPTIONAL Очистить так же и байт статуса (по умолчанию = нет).
     */
    public function clean(bool $full = false)
    {
        $shmop = $this->open();
        $started = $this->getDebugMode() ? microtime(true) : 0;

        // Сперва вычищаем все данные блоками по 1Мб
        $offset = self::HEADER_SIZE;
        $length = 0x100000;
        $fill   = str_repeat(chr(0), $length);
        $size   = $this->getBehavior()->getSize() - $offset;
        while ($size > $length) {
            $this->writeShmop($shmop, $offset, $fill);
            $size -= $length;
        }

        // Чистим оставшийся кусок, меньший 1Мб.
        if ($size > 0) {
            $fill = str_repeat(chr(0), $size);
            $this->writeShmop($shmop, $offset, $fill);
        }

        // В конце - вычищаем заголовок.
        $offset = $full ? 0 : self::STATUS_SIZE;
        $size   = self::HEADER_SIZE - $offset;
        $fill   = str_repeat(chr(0), $size);
        $this->writeShmop($shmop, $offset, $fill);
        $this->setDebugData('CACHE CLEAN', $started);
    }

    /**
     * Удаление блока разделяемой памяти.
     *
     * @param boolean $clean OPTIONAL Нужно ли сперва полностью очистить все (по умолчанию = да).
     *
     * @throws \TextMedia\ShmStorage\Exception
     */
    public function remove(bool $clean = true)
    {
        if ($clean) {
            $this->clean(true);
        }

        if (@shmop_delete($this->open()) !== true) {
            throw new Exception('Не удалось удалить блок разделяемой памяти.', Exception::FAILED_DELETE_SHMOP);
        }
    }

    /**
     * Получение объекта настроек/поведения кэша.
     *
     * @return \TextMedia\ShmCache\Behavior
     */
    public function getBehavior(): Behavior
    {
        return $this->behavior;
    }

    /* ****************************************************************************************** *\
     * УПРАВЛЕНИЕ СТАТУСОМ КЭША
    \* ****************************************************************************************** */

    /**
     * Установка статуса кэша.
     *
     * @throws \TextMedia\ShmStorage\Exception
     *
     * @param integer $status Значение статуса.
     */
    public function setStatus(int $status)
    {
        if (self::STATUS_EMPTY > $status or $status > self::STATUS_CORRUPT) {
            throw new Exception('Неправильное значение статуса.', Exception::INVALID_STATUS_VALUE);
        }

        $offset = 0;
        $this->writeShmop($this->open(), $offset, chr($status));
    }

    /**
     * Получение статуса кэша.
     *
     * @throws \TextMedia\ShmStorage\Exception
     *
     * @return integer
     */
    public function getStatus(): int
    {
        $offset = 0;
        $status = ord($this->readShmop($this->open(), $offset, self::STATUS_SIZE));
        if (self::STATUS_EMPTY > $status or $status > self::STATUS_CORRUPT) {
            throw new Exception('Неправильное значение статуса.', Exception::INVALID_STATUS_VALUE);
        } elseif (self::STATUS_EMPTY === $status) {
            $this->getBehavior()->onEmpty($this);
        }

        return $status;
    }

    /**
     * Проверка, является ли хранилище пустым.
     *
     * @return boolean
     */
    public function isEmpty(): bool
    {
        return ($this->getStatus() === self::STATUS_EMPTY);
    }

    /**
     * Проверка, находится ли хранилище на "прогреве".
     *
     * @return boolean
     */
    public function isWarmup(): bool
    {
        return ($this->getStatus() === self::STATUS_WARMUP);
    }

    /**
     * Проверка, готово ли хранилище.
     *
     * @return boolean
     */
    public function isReady(): bool
    {
        return ($this->getStatus() === self::STATUS_READY);
    }

    /**
     * Проверка, не повреждено ли хранилище.
     *
     * @return boolean
     */
    public function isCorrupt(): bool
    {
        return ($this->getStatus() === self::STATUS_CORRUPT);
    }

    /* ****************************************************************************************** *\
     * ИНТЕРФЕЙСНЫЕ МЕТОДЫ
    \* ****************************************************************************************** */

    /**
     * "Прогрев" блока разделяемой памяти.
     * Не выполняет проверку статуса, т.к. это обязан делать скрипт, инициализирующий "прогрев".
     *
     * @param boolean $clean OPTIONAL Нужно ли сперва полностью очистить все (по умолчанию = да).
     */
    public function warmup(bool $clean = true)
    {
        $shmop = $this->open();
        $this->setStatus(self::STATUS_WARMUP);
        if ($clean) {
            $this->clean(false);
        }

        $started = $this->getDebugMode() ? microtime(true) : 0;
        $onStart = (object) [
            'time'    => microtime(true),
            'memory'  => memory_get_peak_usage(true),
            'records' => 0,
            'size'    => 0,
        ];

        $offset = self::HEADER_SIZE;
        $data   = $this->getBehavior()->getData();
        $count  = 0;
        foreach ($data as $key => $value) {
            $packed  = $this->getBehavior()->packData($key, $value);
            $data    = self::packNumber(strlen($key), 1) . $key
                     . self::packNumber(strlen($packed), $this->getBehavior()->getLength()) . $packed;
            $this->writeShmop($shmop, $offset, $data);
            $count++;
        }
        $this->setDebugData('CACHE WARMUP', $started);

        $onReady = (object) [
            'time'    => microtime(true),
            'memory'  => memory_get_peak_usage(true),
            'records' => $count,
            'size'    => $offset - self::HEADER_SIZE,
        ];
        $this->getBehavior()->onWarmed($this, $onStart, $onReady);

        $this->setStatus(self::STATUS_READY);
    }

    /**
     * Чтение данных из разделяемой памяти по ключу.
     *
     * @param string $key Ключ.
     *
     * @return mixed
     */
    public function getItem(string $key)
    {
        $offset = $this->getOffset($key);
        return $this->unpackValue($this->open(), $key, $offset);
    }

    /**
     * Чтение данных из разделяемой памяти по множеству ключей.
     *
     * @param array   $keys          Ключи.
     * @param boolean $ignoreMissing OPTIONAL Игнорировать отсутствующие вместо исключения (по умолчанию = да).
     *
     * @return array
     */
    public function getItems(array $keys, bool $ignoreMissing = true): array
    {
        $result = [];
        foreach ($keys as $key) {
            try {
                $result[$key] = $this->getItem($key);
            } catch (Exception $ex) {
                if ($ignoreMissing and $ex->getCode() === $ex::FAILED_SEARCH_KEY) {
                    unset($result[$key]);
                } else {
                    throw $ex;
                }
            }
        }
        return $result;
    }

    /**
     * Чтение смещения по ключу из таблицы (всей таблицы данных из разделяемой памяти).
     *
     * @param string $key OPTIONAL Ключ (если не указан, будет возвращена вся таблица).
     *
     * @throws \TextMedia\ShmStorage\Exception Если хранилище не готово к работе или ключ в ней отсутствует.
     *
     * @return integer|ArrayObject
     */
    public function getOffset(string $key = null)
    {
        static $tables = null;

        if (!$this->isReady()) {
            throw new Exception('Кэш не готов к чтению.', Exception::CACHE_NOT_READY);
        }

        $hash = $this->getBehavior()->getId();
        if (!isset($tables[$hash])) { // Сформируем таблицу, если еще не определена.
            $started = $this->getDebugMode() ? microtime(true) : 0;
            $onStart = (object) [
                'time'    => microtime(true),
                'memory'  => memory_get_peak_usage(true),
                'records' => 0,
                'size'    => 0,
            ];

            $tables[$hash] = new ArrayObject;
            $offset = self::HEADER_SIZE;
            $shmop  = $this->open();
            $count  = 0;
            while (true) {
                // Пробуем прочитать ключ.
                $key0 = $this->readValue($shmop, $offset, 1);
                if (is_null($key0)) {
                    break;
                }

                // Сохраним ключ/смещение в таблице.
                list($key1, $key2) = $this->getKeys($key0);
                if (!isset($tables[$hash][$key1])) {
                    $tables[$hash]->offsetSet($key1, []);
                }
                $tables[$hash][$key1][$key2] = $offset;

                // Проверим данные и перейдем к следующему ключу.
                $this->unpackValue($shmop, $key0, $offset);
                $count++;
            }

            $onReady = (object) [
                'time'    => microtime(true),
                'memory'  => memory_get_peak_usage(true),
                'records' => $count,
                'size'    => $offset - self::HEADER_SIZE,
            ];
            $this->getBehavior()->onIndexed($this, $onStart, $onReady);
            $this->setDebugData('TABLE CREATION', $started);
        }

        if (!is_null($key)) { // Вернем смещение по ключу, если указан.
            $started = $this->getDebugMode() ? microtime(true) : 0;

            list($key1, $key2) = $this->getKeys($key);
            if (!isset($tables[$hash][$key1]) or !isset($tables[$hash][$key1][$key2])) {
                throw new Exception('Указанный ключ отсутствует в таблице.', Exception::FAILED_SEARCH_KEY);
            }

            $this->setDebugData('OFFSET SEARCH', $started);
            return $tables[$hash][$key1][$key2];
        }

        return $tables[$hash];
    }

    /* ****************************************************************************************** *\
     * МЕТОДЫ ДЛЯ ОТЛАДКИ
    \* ****************************************************************************************** */

    /**
     * Установка флага режима отладки.
     *
     * @param boolean $flag Значение флага.
     */
    public function setDebugMode(bool $flag)
    {
        $this->debugFlag = $flag;
    }

    /**
     * Проверка, включен ли режим отладки.
     *
     * @return boolean
     */
    public function getDebugMode(): bool
    {
        return $this->debugFlag;
    }

    /**
     * Возврат данных отладки.
     *
     * @return array
     */
    public function getDebugData(): array
    {
        return $this->debugData;
    }

    /**
     * Сохранение даннах отладки.
     *
     * @param string $action  Какое действие выполнялось.
     * @param float  $started Когда было начато выполнение (microtime(true)).
     */
    protected function setDebugData(string $action, float $started)
    {
        if ($this->getDebugMode()) {
            $this->debugData[] = [
                'action' => $action,
                'time'   => sprintf('%1.8f', microtime(true) - $started),
                'memory' => number_format(memory_get_peak_usage(true), 0, '.', ' '),
            ];
        }
    }

    /* ****************************************************************************************** *\
     * ВСПОМОГАТЕЛЬНЫЕ МЕТОДЫ
    \* ****************************************************************************************** */

    /**
     * Получение пути для ключа в таблице.
     *
     * @param string $key Ключ.
     *
     * @return array Пара [$key1, $key2]
     */
    protected function getKeys(string $key): array
    {
        // Смещения не хранятся в таблице непрерывным потоком, а разбиваются на блоки по первому символу:
        //  - смещение по ключу "123" будет храниться в $table[1][123];
        //  - смещение по ключу "abc" будет храниться в $table['a']['abc'].
        $key1 = $key{0};
        $key2 = $key;
        if (preg_match('#^\d+$#', $key)) {
            $key1 = (int) $key1;
            $key2 = (int) $key2;
        } else {
            $key1 = ord($key1);
            $key2 = crc32($key2);
        }
        return [$key1, $key2];
    }

    /**
     * Чтение данных из разделяемой памяти.
     * Перехватывает только исключения из собственной области имен.
     *
     * @param resource $shmop  Идентификатор блока разделяемой памяти
     * @param string   $key    Ключ.
     * @param integer  $offset Смещение.
     *
     * @throws \TextMedia\ShmStorage\Exception Если данные оказались "битыми".
     *
     * @return mixed
     */
    protected function unpackValue($shmop, string $key, &$offset)
    {
        $value = $this->readValue($shmop, $offset, $this->getBehavior()->getLength());
        try {
            $value = $this->getBehavior()->unpackData($key, $value);
        } catch (Exception $ex) {
            $this->setStatus(self::STATUS_CORRUPT);
            $this->getBehavior()->onCorrupt($this, $key, $value);
            throw $ex;
        }
        return $value;
    }

    /**
     * Чтение данных из блока разделяемой памяти.
     *
     * @param resource $shmop  Ресурс блока разделяемой памяти.
     * @param integer  $offset Смещение.
     * @param integer  $length Длина.
     *
     * @throws \TextMedia\ShmStorage\Exception Если не удалось прочитать данные в полном объеме.
     *
     * @return string
     */
    protected function readShmop($shmop, int &$offset, int $length): string
    {
        $data = @shmop_read($shmop, $offset, $length);
        if ($data === false or !is_string($data) or strlen($data) !== $length) {
            $this->setStatus(self::STATUS_CORRUPT);
            throw new Exception('Ошибка чтения из разделяемой памяти.', Exception::FAILED_READ_SHMOP);
        }

        $offset += $length;
        return $data;
    }

    /**
     * Чтение данных из блока разделяемой памяти.
     *
     * @param resource $shmop  Ресурс блока разделяемой памяти.
     * @param integer  $offset Смещение.
     * @param integer  $length Длина.
     *
     * @return string|NULL
     */
    protected function readValue($shmop, int &$offset, int $length)
    {
        $length = $this->readShmop($shmop, $offset, $length);
        $length = self::unpackNumber($length);
        if (0 === $length) {
            return null;
        }

        $value  = $this->readShmop($shmop, $offset, $length);
        return $value;
    }

    /**
     * Запись данных в блок разделяемой памяти.
     *
     * @param resource $shmop  Ресурс блока разделяемой памяти.
     * @param integer  $offset Смещение.
     * @param string   $data   Данные.
     *
     * @throws \TextMedia\ShmStorage\Exception Если не удалось прочитать дааные в полном объеме.
     *
     * @return integer
     */
    protected function writeShmop($shmop, int &$offset, string $data): int
    {
        $length = strlen($data);
        if (@shmop_write($shmop, $data, $offset) !== $length) {
            throw new Exception('Ошибка записи в разделяемую память.', Exception::FAILED_WRITE_SHMOP);
        }

        $offset += $length;
        return $length;
    }
}
